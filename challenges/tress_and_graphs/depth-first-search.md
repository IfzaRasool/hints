# Trees and Graphs: Depth-First Search

<!-- Hint 1 -->
<details>
  <summary><b>Hint:</b>&nbsp;Basis of the problem</summary><br>

As its name implies, the strategy followed by a depth-first search (DFS) is to search **deeper** in the graph first whenever possible. The algorithm begins with an arbitrary node (in this challenge, we will always start with the node "0") and goes down a **particular branch** through an undiscovered adjacent node until we reach a dead end. Then it backs up and goes as deep as possible again.

The depth-first search was invented in the 19th century for solving mazes. The animation below shows how a DFS algorithm traverses in a maze:<br>
<img src="images/trees_and_graphs/depth-first-search/dfs-1.gif"  width="500"><br>


In basic DFS, we need two main variables:<br>
&emsp;\- A **stack** to perform the depth-first order. Whenever we reach a dead-end node, we must get back to the previous node; this technique is called backtracking. We need a stack to backtrack.<br>
&emsp;\- An array to mark whether or not each node has been discovered. In contrast to the tree data structure, graph nodes can be **cyclic**. We cannot allow any node to be visited over and over.<br>

In addition, we need to return an array to keep track of the path in which we proceed. Therefore, we need another variable for the traversal path.

The depth-first search is a naturally recursive algorithm, and the recursive implemention of DFS is easier and more readable than the iterative one.  With recursive implementation, stack and backtracking are handled spontaneously by recursion. We will cover each of them in this guide.

---

<br>

#### Recursive Solution

<img src="images/trees_and_graphs/depth-first-search/dfs-2.png"  width="800"><br>

The recursive implementation is quite simple:<br>
<b>`Hint:`&nbsp;Use function parameters to pass the current node, the graph data, the discovered array, and the path.<br>
<b>`Hint:`&nbsp;No need to define a stack; the "recursive call stack" handles backtracking.<br>
<b>`Hint:`&nbsp;Start with the root node "0".<br>
<b>`Hint:`&nbsp;At each recursive step, simply push the current node to the path array.<br>
<b>`Hint:`&nbsp;Then, recursively call DFS for each undiscovered adjacent node one by one.<br>

---

<br>

#### Iterative Solution

The non-recursive implementation is quite similar to the iterative breadth-first search we implemented in the previous challenge, but it differs in two ways:<br>
&emsp;\- It uses a stack instead of a **queue**.<br>
&emsp;\- The test cases are defined based on the recursive result. To make the iterative DFS yield the same result as the recursive DFS, you need to push the nodes to the stack in reverse order. **The last node discovered must be the first visited (last-in-first-out).**<br>
<img src="images/trees_and_graphs/depth-first-search/dfs-3.png"  width="800">

In the iterative approach, first we push all the adjacent nodes into the stack. Then, it handles the head of the stack first. That is, the first node you handle is the last adjacent node of its current location.<br>
In the recursive approach, we handle each node when we see it. Thus, the first node you handle branches out first.

We explain below why the adjacent nodes must be processed in reverse order:<br>
<img src="images/trees_and_graphs/depth-first-search/dfs-4.png"  width="800">

For the iterative implementation:<br>
<b>`Hint:`&nbsp;Start with the root node "0".<br>
<b>`Hint:`&nbsp;At each loop step, pop (i.e. remove) the last node from the stack. Then, push each adjacent node of that node into the stack in reverse order.<br>
<b>`Hint:`&nbsp;Loop the same process until there are no more undiscovered nodes.<br>



</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2: [CODE]</b>&nbsp;Iterative implementation</summary><br>

The structure is the same as the BFS implementation:

```ruby
def depth_first_search(graph)
  discovered = Array.new(graph.keys.length, false)
  # 
  stack = []
  path = []
  # Start traversing with the node "0"
  stack.push(0)
  # Mark the node "0" discovered
  discovered[0] = true

  # Loop until there are no more undiscovered nodes
  until stack.empty?
    # Implement your solution here
  end
  path
end
```

<b>`Hint:`&nbsp;First, push the node "0" to the end of the stack.<br>
<b>`Hint:`&nbsp;At each loop step:<br>
&emsp;\- Pop (i.e. remove) the last element from the stack. Keep it; it is the current node.<br>
&emsp;\- Next, push the current node to the path array. We will return the path as the overall result.<br>
&emsp;\- Then, explore each adjacent node in *reverse order* from the current node. Push each undiscovered adjacent node to the end of the stack and mark it discovered.<br>
<b>`Hint:`&nbsp;Continue through the loop until the stack is empty.<br>

</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3: [CODE]</b>&nbsp;Recursive Implementation</summary><br>

We need to use a **path** array, and a **discovered** array. The recommended approach is to use a helper method to operate the recursion. Therefore, we will use the **dfs_visit** method to handle the recursive calls.

```ruby
def depth_first_search(graph)
  discovered = Array.new(graph.keys.length, false)
  path = []
  # Start traversing with the node "0"
  dfs_visit(0, graph, discovered, path)
  path
end

def dfs_visit(current_node, graph, discovered, path)
  # implement your solution here
end
```


<b>`Hint:`&nbsp;Start with the node "0".<br>
<b>`Hint:`&nbsp;Each recursive call is responsible for processing its current node and for each undiscovered adjacent node.<br>
<b>`Hint:`&nbsp;Continue to recur until there are no more undiscovered adjacent nodes.<br>

</details>
<!-- Hint 3 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution (Iterative)</summary><br>

```ruby
def depth_first_search(graph)
  discovered = Array.new(graph.keys.length, false)
  path = []
  stack = []
  stack.push(0)
  discovered[0] = true
  until stack.empty?
    current = stack.pop
    # We're processing the current element at this point. So, push the current node to the result path 
    path.push(current)
    # Explore the adjacent nodes of the current node in reverse order
    graph[current].reverse_each do |adjacent|
      # If it had not been discovered previously, push it to the end of the stack and mark it discoreved
      unless discovered[adjacent]
        stack.push(adjacent)
        discovered[adjacent] = true
      end
    end
  end
  path
end
```

</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Advanced Solution (Recursive) </summary><br>

```ruby
def depth_first_search(graph)
  discovered = Array.new(graph.keys.length, false)
  path = []
  dfs_visit(0, graph, discovered, path)
  path
end

def dfs_visit(current_node, graph, discovered, path)
  discovered[current_node] = true
  # We're processing the current node at this point. So, push the current node to the result path
  path.push(current_node)
  # Recursively branch out each undiscovered adjacent node one by one
  graph[current_node].each do |adjacent_node| 
    unless discovered[adjacent_node]
      dfs_visit(adjacent_node, graph, discovered, path)
    end
  end
end
```

</details>
<!-- Solution 2 -->

